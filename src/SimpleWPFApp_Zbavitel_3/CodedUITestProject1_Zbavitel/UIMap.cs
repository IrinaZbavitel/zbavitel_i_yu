﻿namespace CodedUITestProject1_Zbavitel
{
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }
        /// <summary>
        /// SimpleAppTest_Zbavitel - Используйте "SimpleAppTest_ZbavitelParams" для передачи параметров в этот метод.
        /// </summary>
        public void ModifiedSimpleAppTest_Zbavitel()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            #endregion

            // Запуск "D:\Zbavitel\Projects\SimpleWPFApp_Zbavitel_3\SimpleWPFApp_Zbavitel_3\bin\Debug\SimpleWPFApp_Zbavitel_3.exe"
            ApplicationUnderTest uIMainWindowWindow = ApplicationUnderTest.Launch(this.SimpleAppTest_ZbavitelParams.UIMainWindowWindowExePath, this.SimpleAppTest_ZbavitelParams.UIMainWindowWindowAlternateExePath);

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(17, 5));
            uICheckBoxCheckBox.WaitForControlEnabled();
            // Очистить "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTest_ZbavitelParams.UICheckBoxCheckBoxChecked;
        }

        public virtual SimpleAppTest_ZbavitelParams SimpleAppTest_ZbavitelParams
        {
            get
            {
                if ((this.mSimpleAppTest_ZbavitelParams == null))
                {
                    this.mSimpleAppTest_ZbavitelParams = new SimpleAppTest_ZbavitelParams();
                }
                return this.mSimpleAppTest_ZbavitelParams;
            }
        }

        private SimpleAppTest_ZbavitelParams mSimpleAppTest_ZbavitelParams;
    }
    /// <summary>
    /// Параметры для передачи в "SimpleAppTest_Zbavitel"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class SimpleAppTest_ZbavitelParams
    {

        #region Fields
        /// <summary>
        /// Запуск "D:\Zbavitel\Projects\SimpleWPFApp_Zbavitel_3\SimpleWPFApp_Zbavitel_3\bin\Debug\SimpleWPFApp_Zbavitel_3.exe"
        /// </summary>
        public string UIMainWindowWindowExePath = "D:\\Zbavitel\\Projects\\SimpleWPFApp_Zbavitel_3\\SimpleWPFApp_Zbavitel_3\\bin\\Debug\\Si" +
            "mpleWPFApp_Zbavitel_3.exe";

        /// <summary>
        /// Запуск "D:\Zbavitel\Projects\SimpleWPFApp_Zbavitel_3\SimpleWPFApp_Zbavitel_3\bin\Debug\SimpleWPFApp_Zbavitel_3.exe"
        /// </summary>
        public string UIMainWindowWindowAlternateExePath = "D:\\Zbavitel\\Projects\\SimpleWPFApp_Zbavitel_3\\SimpleWPFApp_Zbavitel_3\\bin\\Debug\\Si" +
            "mpleWPFApp_Zbavitel_3.exe";

        /// <summary>
        /// Очистить "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = false;
        #endregion
    }
}
