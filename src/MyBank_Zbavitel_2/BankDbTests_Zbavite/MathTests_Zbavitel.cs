using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankDb_Zbavitel;
using System.Collections.Generic;
using System.Reflection;
using System;
namespace BankDbTests_Zbavitel
{
    [TestClass]

    public class MathsTests_Zbavitel
    {

        [DataTestMethod]
        [DataRow(1, 1, 2)]
        [DataRow(2, 2, 4)]
        [DataRow(3, 3, 6)]
        [DataRow(0, 0, 1)] // The test run with this row fails
        public void AddIntegers_FromDataRowTest(int x, int y, int expected)
        {
            var target = new Maths_Zbavitel();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

        public static IEnumerable<object[]> AdditionData
        {
            get
            {
                return new[]
                {
            new object[] { 1, 1, 2 },
            new object[] { 2, 2, 4 },
            new object[] { 3, 3, 6 },
            new object[] { 0, 0, 1 }, // The test run with this row fails
                };
            }
        }

        public static string GetCustomDynamicDataDisplayName(MethodInfo methodInfo, object[] data)
        {
            return string.Format("DynamicDataTestMethod {0} with {1} parameters", methodInfo.Name, data.Length);
        }

        [TestMethod]

        [DynamicData(nameof(AdditionData), DynamicDataDisplayName = nameof(GetCustomDynamicDataDisplayName))]
        public void AddIntegers_FromDynamicDataTest(int x, int y, int expected)
        {
            var target = new Maths_Zbavitel();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

        public TestContext TestContext { get; set; }

        [TestMethod]

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @".\TestData.csv", "TestData#csv", DataAccessMethod.Sequential)]

        public void AddIntegers_FromDataSourceTest()
        {
            var target = new Maths_Zbavitel();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
